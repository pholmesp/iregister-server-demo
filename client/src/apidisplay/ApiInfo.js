import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import NavBar from '../NavBar';
import { User } from '../User';
import { Main } from '../Main';
import ApiDocDisplay  from './ApiDocDisplay';


import './ApiInfo.css';

const path = 'http://pte.novasoftware.com.au:3090';


class ApiInfo extends Component {

  componentDidMount() {
    this.getData();
}

  getData() {
   const that = this;
   const link = `/allservices`;
   axios({
     method: 'get',
     baseURL: path,
     url: link,
   }).then((response) => {
    //  console.log('Get data: ', response);
     if (response.data.length > 0) {
       const info = {};
       const data = response.data;
      //  console.log(data);

       this.props.setApiDocs(data);
       console.log(this.props.apidocs);
     } else console.log("Get data error ");
   }).catch((error) => {
     console.log('error on .catch', error);
   });
 }

  render() {
    return (
      <div className="container">
        <NavBar />
        <div className="row apidocs">
        { /* <Main changeUsername={() => this.props.setName("Jevy")} />
          <User username={this.props.user.name} /> */}
          <h2>API Doc</h2>
          <hr/>
          <ApiDocDisplay docs={this.props.apidocs}/>
        </div>

      </div>

    );
  }
}

const mapStateToProps = (state) =>{
  return {
    user: state.user,
    math: state.math,
    apidocs : state.apidocs
  }
};

const mapDispatchToProps = (dispatch) =>{
  return {
    setName: (name) => {
      dispatch({
        type: "SET_NAME",
        payload: name
      });
    },

    setApiDocs: (data) => {
      dispatch(
        {
          type: "SET_APIDOCS",
          payload: data
        }
      );
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ApiInfo);
