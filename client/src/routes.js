import React from 'react';
import { Route, IndexRoute } from 'react-router';
// import LoginPage from './components/login/LoginPage';
import App from './App';
import ApiInfo from './apidisplay/ApiInfo';

import ApiNew from './apicreate/ApiNew';
import ApiNewInput from './apicreate/ApiNewInput';

import './index.css';
// import SignupPage from './components/signup/SignupPage';
// import WaitPage from './components/WaitPage';
// import Dashboard from './components/Dashboard/Dashboard';
// import LogoutComponent from './components/logout/Logout';
// import NotFound from './components/PageNotFound';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={ApiInfo} />
    <Route path="/apiInfo" component={ApiInfo} />
    <Route path="/newApi" component={ApiNew} />
  </Route>

);

// <Route path="signup" component={SignupPage} />
// <Route path="wait" component={WaitPage} />
// <Route path="login" component={LoginPage} />
// <Route path="logout" component={LogoutComponent} />
// <Route path="dashboard/:username" component={Dashboard} />
