import React, { Component } from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import axios from 'axios';

import NavBar from '../NavBar';
import ApiNewInput  from './ApiNewInput';


const path = 'http://pte.novasoftware.com.au:3090';

class ApiNew extends Component {
  constructor(props) {
  super(props);
  this.state = {
    serviceName: '',
    error: {},
  };
  this.onChange = this.onChange.bind(this);
  this.onSubmit = this.onSubmit.bind(this);
}

onChange(e) {
  this.setState({ [e.target.name]: e.target.value });
}

isValid() {
  // const that = this;
  // const { errors, isValid } = validateLoginInput(this.state);
  // if (!isValid) {
  //   that.setState({ error: errors });
  //   return false;
  // }
  return true;
}

onSubmit(e) {
  e.preventDefault();
  if (this.isValid()) {
    const that = this;
    console.log(that);
    // return () => axios.post('/login', userData);
    const data = {};
    data.api = that.state.serviceName;
    axios.post(path+'/service', data).then((response) =>{
      if(response.data){
        browserHistory.push(`apiInfo`);
      }else{
        console.log("Create Service Fail");
      }
    });
  }
}

  render() {
    return (
      <div className="container">
        <NavBar />
        <div className="row api-new">
          <h2>Create a New Api Service</h2>
          <hr/>
          <ApiNewInput
            onSubmit={this.onSubmit}
            onChange={this.onChange}
            serviceName={this.state.serviceName}
            error={this.state.error}
          />
        </div>
      </div>

    );
  }
}

const mapStateToProps = (state) =>{
  return {
    user: state.user,
    math: state.math,
    apidocs : state.apidocs
  }
};

const mapDispatchToProps = (dispatch) =>{
  return {
    setName: (name) => {
      dispatch({
        type: "SET_NAME",
        payload: name
      });
    },

    setApiDocs: (data) => {
      dispatch(
        {
          type: "SET_APIDOCS",
          payload: data
        }
      );
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ApiNew);
