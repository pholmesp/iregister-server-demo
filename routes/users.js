var express = require('express');
var router = express.Router();
var passport = require('../config/passport');
var jwt = require('jsonwebtoken');
var exjwt = require('express-jwt');
var bcrypt = require('bcrypt-nodejs');
var config = require('../moudle/config');
var dbServices = require('../moudle/db-service');
//var passport = require('../config/googleAuth');
var LocalStorage = require('node-localstorage').LocalStorage;
  localStorage = new LocalStorage('./scratch');
  var passportJWT = require("passport-jwt");
  var ExtractJwt = passportJWT.ExtractJwt;
var elasticlunr = require('elasticlunr');


/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/signin/:phone', function(req, res, next) {
  var sql = 'select IDUSER, IDSHOP, NAME, PHONE, EMAIL, ADDRESS, CODE, CODEEXPIRE, IMAGE,POSTLIMIT, TYPE, HIDDENEMAIL, HIDDENPHONE from USER where phone = ?';
    dbServices.query(sql, [req.params.phone], function(error, dd){

      if(!error){
        res.json(dd[0])
      }else{
        res.json(error)
      }
    });
});

//User sign up
// router.post('/signup', function(req, res) {
//   //console.log("1 "+req.body.name)
  
//             //console.log("User input: ",username, password)
                   
//   if (!req.body.email || !req.body.password) {
//     res.json({success: false, msg: 'Please pass username and password.'});
//   } 
  
//   else {
//     // Check whether the email is existing or not
//     var sql = 'select * from USER where email = ?';
//     dbServices.query(sql, [req.body.email], function(error, user){
//       //console.log("email: "+req.body.email);
//       //console.log("db: "+user.length);
//       if(error){
//         return res.json();
//       }
//       else if (user.length >0) {
//         return res.json({success: false, msg: 'email existing'});
//         }
//       else  {
//         var sql = 'select * from USER where name = ?';
//         dbServices.query(sql, [req.body.name], function(error, user){
//           if(error){
//             return res.json();
//           }
//           else if (user.length >0){
//             return res.json({success: false, msg: 'username existing'});
//           }
//           else {

          
//         // Encrypt the password
//             // var un = req.body.username;
//             // var pw = req.body.password;
//             //console.log("2 "+req.body.name)
//             bcrypt.genSalt(10, function (err, salt) {
//               if (err) {
//                   return res.json();
//               }

//               bcrypt.hash(req.body.password, salt, null, function (err, hash) {
//                   if (err) {
//                     return res.json();
//                   }
//                   req.body['password'] = hash;
              
//               // save the user
//               var sql = 'insert into USER set ?';
//               //console.log(req.body)
//               dbServices.query(sql, [req.body], function(error){
          
//                 if(error){
//                   return res.json({success: false, msg: error});
//                 }
//                 else{
//                   var sq = 'select * from USER where email = ?'
//                   //console.log()
//                   dbServices.query(sq, [req.body.email], function(error, user){
//                     if(error){
//                       return res.json({success: false, msg: error});
//                     }
                    
//                     else {

                      
//                       ResUser = ({
//                         "idUSER": user[0].idUSER,
//                         "name": user[0].name,
//                         "email": user[0].email,
//                         "phone": user[0].phone,
//                         "postLimit": user[0].postLimit,
//                         "type": user[0].type,
//                         "code": user[0].code,
//                         "codeExpire": user[0].codeExpire,
//                         "image": user[0].image,
//                         "address": user[0].address,
//                         "idSHOP": user[0].idSHOP,
//                         "HIDDENEMAIL": user[0].HIDDENEMAIL,
//                         "HIDDENPHONE": user[0].HIDDENPHONE 
//                     })
//                       res.json({success: true, msg: 'Successfully created the new user', usr: ResUser});
//                     }
//                   })
                  
                  
//                   //console.log("3 "+req.body.name)
//                 }
//               });
//               });
//           });
//         }
//         })
//         }
      
    
        
      
//     });
   
    
//   }
// });

// // User log in
// router.post('/signin', function(req, res) {

//   var sql = 'select PASSWORD from USER where NAME = ?';
//   dbServices.query(sql, [req.body.name], function(error, dd){  
//     console.log(req.body.name)
// // console.log(error)
//  console.log(dd)
//     if(error){
//       res.status(401).send({success: false, msg: 'Authentication failed. User not found.'});
//       //return res.json({success: false, msg: error});
//     }else{
//       //console.log(dd[0].PASSWORD)
//       bcrypt.compare(req.body.password, dd[0].PASSWORD, function (err) {  
//         if (err) {
//           res.status(401).send({success: false, msg: err});
//         }
//         // if user is found and password is right create a token
        
//         // return the information including token as JSON
//         res.json({success: true, msg: 'verified user'});
//     });
//     }
//   });
// });

// router.post('/login',
//   passport.authenticate('local',{ successRedirect: '/',
//   failureRedirect: '/' }));

// router.get('/login', function(req, res, next) {
//   passport.authenticate('local', function(err, user, info) {
//     if (err) { return next(err); }
//     if (!user) { return res.redirect('/login'); }
//     req.logIn(user, function(err) {
//       if (err) { return next(err); }
//       return res.json( user.username);
//     });
//   })(req, res, next);
// });
// const jwtMW = exjwt({
//   secret: 'keyboard cat 4 ever'
// });

router.post("/secret", passport.authenticate('jwtAdmin'), function(req, res){
  res.json("Success! You can not see this without a token");
});

// router.get("/secretDebug",
//   function(req, res, next){
//     console.log(req.get('Authorization'));
//     console.log(ExtractJwt.fromAuthHeaderAsBearerToken());
//     next();
//   }, function(req, res){
//     res.json("debugging");
// });

router.post('/userlogin',
  passport.authenticate(['user', 'admin']),
  (req, res)=> {
    //console.log(req.ip)
    res.json(req.user);
  })
  router.post('/logintest', (req, res, next)=> {
    console.log(req.ips)
    passport.authenticate('user', (err, user, info)=> {
      if (err) 
        { return next(err); }
      else if (!user)
        { return res.json({success: false, msg: "Incorrect account info provided."}); }
      else {
        req.logIn(user, function(err) {
          if (err) 
            { return next(err); }
          else {
            return res.json({success: true, msg: user});
          }
        });
      }
    })(req, res, next);
  });

router.post('/adminlogin',
  passport.authenticate('admin'),
  (req, res)=> {
    
    res.json(req.user);
  });

  router.get('/logout', (req, res)=> {
    //console.log(JSON.parse(localStorage.getItem(req.user.email)));
    //localStorage.removeItem(req.user.email);
    //console.log(JSON.parse(localStorage.getItem(req.user.email)));
    //console.log("a: "+req.user);
    //localStorage.clear();
    req.logOut();
    res.json({'logout':'success'})
    // res.redirect('/');
    console.log('logout');
    //res.json({success: true, msg: 'Successful signed out.'})
    
  });

  // router.post('/pw', (req, res)=> {
  //   bcrypt.genSalt(10, function (err, salt) {
  //     if (err) {
  //         res.json(err);
  //     }

  //     bcrypt.hash(req.body.name, salt, null, function (err, hash) {
  //         if (err) {
  //           res.json(err);
  //         }
  //         else {
  //           res.json({msg: hash});

  //         }
  //       })
  //     })
    
  // });

  // router.post('/pwchange', passport.authenticate('jwt'),function(req, res) {
  //   var sql = 'SELECT * FROM USER where idUSER = ?'
  //   var sq = 'update USER set ? where idUSER = ?'

  //   console.log(req.body.id)
  //   dbServices.query(sql, [req.body.id], function(error, user){  

  //     //console.log(dd)
  //     if(error){
  //         res.json(error)
  
  //     }else if (user.length === 0){
  //       res.json({ message: 'User not found.' });
  //     }else{
  //     //console.log(dd[0].PASSWORD)
  //     //console.log(JSON.parse(JSON.stringify(user[0])))
  //         bcrypt.compare(req.body.oldpassword, user[0].password, function (err, isMatch) {  
  //             if (err) {
  //                 res.json({ message: 'Incorrect password.' });
  //             }else if(!isMatch){
  //               res.json({ message: 'Incorrect password.' });
  //             }else{
  //               bcrypt.genSalt(10, function (err, salt) {
  //                 if (err) {
  //                     res.json(err);
  //                 }
    
  //                 bcrypt.hash(req.body.newpassword, salt, null, function (err, hash) {
  //                     if (err) {
  //                       res.json(err);
  //                     }
                      
                  
  //                 // update the password
  //                 var sq = 'update USER set ? where idUSER = ?'
  //                 //console.log(req.body)
  //                 dbServices.query(sq, [{password: hash},req.body.id], function(error){
              
  //                   if(error){
  //                     res.json(error);
  //                   }
  //                   else{
  //                     res.json({ message: 'Success' })
                      
  //                   }
  //                 });
  //                 });
  //             });
                
                  
  //             }
  //         });

  //     }
  // });


  //   })

// router.post('/login', 
//   passport.authenticate('local', function(req, res, next) {
//   if(req.user) {
//       res.json(req.user)

//   } else {
//       //handle errors here, decide what you want to send back to your front end
//       //so that it knows the user wasn't found
//       res.statusCode = 503;
//       res.send({message: 'Not Found'})
//   }
// }));

// router.get('/login', (req,res)=>{
//   console.log("redirect from passport")
//   res.json({"error":"Invalid username or password"})
// })

// GET /auth/google
//   Use passport.authenticate() as route middleware to authenticate the
//   request.  The first step in Google authentication will involve
//   redirecting the user to google.com.  After authorization, Google
//   will redirect the user back to this application at /auth/google/callback
// router.get('/auth/google',
// passport.authenticate('google', { scope: [
//         'https://www.googleapis.com/auth/userinfo.profile',
//         'https://www.googleapis.com/auth/userinfo.email'
// ] }));

// GET /auth/google/callback
//   Use passport.authenticate() as route middleware to authenticate the
//   request.  If authentication fails, the user will be redirected back to the
//   login page.  Otherwise, the primary route function function will be called,
//   which, in this example, will redirect the user to the home page.
// router.get('/auth/google/callback', 
// passport.authenticate('google', {failureRedirect: '/login'}), //, { failureRedirect: '/users/auth/google' }
//   function(req, res) {
//     //res.render("loggedin", {user : req.user});
//     //res.send('You have reached the callback URI');
//     res.redirect('/');
//   });

// router.get('/auth/google/logout', (req, res)=> {
//   req.logOut();
//   res.redirect('/');
//   localStorage.removeItem(user.email);
// });
  // Redirect the user to Facebook for authentication.  When complete,
// Facebook will redirect the user back to the application at
//     /auth/facebook/callback
// router.get('/auth/facebook', passport.authenticate('facebook', {scope: 'read_stream'}));

// Facebook will redirect the user to this URL after approval.  Finish the
// authentication process by attempting to obtain an access token.  If
// access was granted, the user will be logged in.  Otherwise,
// authentication has failed.
// router.get('/auth/facebook/callback',
//   passport.authenticate('facebook', { successRedirect: '/',
//                                       failureRedirect: '/login' }));



                             
module.exports = router;
