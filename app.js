var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var https = require('https');
var fs = require('fs');
var cors = require('cors')
var path = require('path')

var routes = require('./routes/index');
var users = require('./routes/users');
var upload = require('./routes/upload');

const WebSocket = require('ws');

var certOptions = {
  key: fs.readFileSync(path.resolve('server.key')),
  cert: fs.readFileSync(path.resolve('server.crt'))
}


var app = express();
var passport = require('passport'),
    session = require("express-session"),
    bodyParser = require("body-parser");




//app.use(cors({allowedHeaders:"Origin, X-Requested-With, Content-Type, Authorization, Accept, Authorization"}))
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://localhost:3001");
  // res.header("Access-Control-Allow-Credentials", "true");
  // res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  // // res.header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers,Authorization,Origin, X-Requested-With, Content-Type, Accept");
  // res.header("Access-Control-Allow-Headers", "Access-Control-Allow-Origin, Access-Control-Allow-Headers, Authorization, Origin, X-Requested-With, Content-Type");
  res.header("Access-Control-Allow-Methods", 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Authorization, Accept, Authorization");
  return next();
});  


// view engine setup_Mingjing
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.static(path.join(__dirname, 'client/build')));

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'log')));





app.use(express.static("public"));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({ 
  secret: "anything", 
  resave: true,
  saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(bodyParser.json())   

// app.all('*', ensureSecure); // at top of routing calls

app.use('/', cors({allowedHeaders:"Origin, X-Requested-With, Content-Type, Authorization, Accept, Authorization"}), routes);
app.use('/users', users);
app.use('/upload', upload);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


// Create https server & run
// var server = https.createServer({
//     key: fs.readFileSync('server.key'),
//     cert: fs.readFileSync('server.crt')
// }, app).listen(app.get('port'), function() {
//     console.log('API Server Started On Port %d', app.get('port'));
// });
var server = https.createServer(certOptions, app).listen(3002);

function ensureSecure(req, res, next){
  if(req.secure){
    return next();
  };
  // handle port numbers if you need non defaults
  // res.redirect('https://' + req.host + req.url); // express 3.x
  res.redirect('https://' + req.hostname +':'+app.get('port')+ req.url); // express 4.x
}

const wss = new WebSocket.Server({server: server});


var clientMap = new Map();
var messageCacheMap = new Map();
wss.on('connection', function connection(ws) {
  // console.log(ws)
  // client = JSON.stringify(ws)
  // clientMap.set()
  ws.on('message', function incoming(message) {
    console.log(message)  
    var message = JSON.parse(message)
    if(message.type=='conn'){
      console.log(message.idUser + 'log on...')
      clientMap.set(message.idUser, ws)
      var hisMessage = messageCacheMap.get(message.idUser)
      if(hisMessage){
        ws.send(JSON.stringify(hisMessage))
      }

    }else{
      var friendWS = clientMap.get(message.to)
      if(friendWS){
        //customer online
        console.log('client '+message.to +
        ' on line, send message from '+ message.from + ' to ' + message.to)
        friendWS.send('['+JSON.stringify(message)+']')
      }else{
        console.log('client '+message.to + ' off line')
        //client offline, store message
        var hisMessage = messageCacheMap.get(message.to)
        if(hisMessage){
          hisMessage.push(message)
          messageCacheMap.set(message.to, hisMessage)
        }else{
          messageCacheMap.set(message.to, [message])
        }
      }
    }
    // console.log(clientMap)
    // console.log(messageCacheMap)
    // ws.send(message);
    // console.log('received: %s', message);
  });

  // ws.send('connected...');
});


//https.createServer(certOptions, app).listen(3002)  
  
module.exports = app;
